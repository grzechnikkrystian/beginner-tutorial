<h1>Edycja szczegółów</h1>

<form action="/customers/{{$customer->id}}" method="post">

    @method('PATCH')

    @include('customer.form')

    <button> Zapisz dane customera </button>

</form>
