@extends('app')


@section('title', 'services')

@section('content')
    <h1> Welcome to laravel 6 from services</h1>

    <form action="/service" method="post">
<input type="text" name="name" autocomplete="off">
@csrf
<button>Add</button>
    </form>

  <p style="color: red">@error('name') {{$message}} @enderror</p>

    <ul>
        @forelse($services as $service)

            <li>{{($service->name)}}</li>
            @empty
                <li>No services</li>
            @endforelse
    </ul>
@endsection
