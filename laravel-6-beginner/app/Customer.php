<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Customer extends Model
{

    protected $fillable = ['name', 'email'];

}
